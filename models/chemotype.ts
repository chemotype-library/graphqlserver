import { Schema, model } from 'mongoose';

const chemotypeSchema = new Schema({
    name: String,
    genoId: String,
    phenoId: String,
    originId: String //make 2 types (person and place)
});

export default model('chemotype', chemotypeSchema, 'chemotype');
