import { Schema, model } from 'mongoose';

const chemicalSchema = new Schema({
    name: String,
    formula: String,
    medical: String,
    benefit: String,
    risk: String,
    type: String,
});

export default model('chemical', chemicalSchema, 'chemical');
