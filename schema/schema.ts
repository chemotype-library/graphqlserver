import Chemo from '../models/chemotype';
import Geno from '../models/genotype';
import Pheno from '../models/phenotype';
import Profile from '../models/profile';
import Chemical from '../models/chemical';
import Origin from '../models/origin';
import _ from 'lodash';

const graphql =require('graphql');
const {
    GraphQLObjectType,
    GraphQLString,
    GraphQLSchema,
    GraphQLID,
    GraphQLList,
    GraphQLNonNull,
} = graphql;

const ChemoObjectType = new GraphQLObjectType({
    name: 'Chemotype',
    fields: ( ) => ({
        id: { type: GraphQLID },
        name: { type: GraphQLString },
        geno: { 
            type: GenoObjectType,
            resolve(parent: { genoId: any; }, _args: any){
                return Geno.findById(parent.genoId);
            }
         },
         pheno: { 
            type: PhenoObjectType,
            resolve(parent: { phenoId: any; }, _args: any){
                return Pheno.findById(parent.phenoId);
            } 
        },
        origin: {
            type: OriginObjectType,
            resolve(parent: { originId: any; }, _args: any){
                return Origin.findById(parent.originId);
            }
        },
        profiles: {
            type: new GraphQLList(ProfileObjectType),
            resolve(parent: { id: any; }, _args: any){
            }
        }
    })
});

const PhenoObjectType = new GraphQLObjectType({
    name: 'Phenotype',
    fields: ( ) => ({
        id: { type: GraphQLID },
        climate: { type: GraphQLString },
        behavior: { type: GraphQLString } 
    })
});

const GenoObjectType = new GraphQLObjectType({
    name: 'Genotype',
    fields: ( ) => ({
        id: { type: GraphQLID },
        genome: { type: GraphQLString },
        maGenoId: { type: GraphQLID },
        paGenoId: { type: GraphQLID }
    })
});

const ProfileObjectType = new GraphQLObjectType({
    name: 'Profile',
    fields: ( ) => ({
        id: { type: GraphQLID },
        chemo: { type: ChemoObjectType,
            resolve(parent: { chemoId: any; }, _args: any){
                return Chemo.findById(parent.chemoId);
            }
        },
        chemical: { type: ChemicalObjectType,
            resolve(parent: { chemicalId: any; }, _args: any){
                return Chemical.findById(parent.chemicalId);
            }
        }
    })
});

const ChemicalObjectType = new GraphQLObjectType({
    name: 'Chemical',
    fields: () => ({
        id: { type: GraphQLID },
        name: { type: GraphQLString },
        formula: { type: GraphQLString },
        medical: { type: GraphQLString },
        benefit: { type: GraphQLString },
        risk: { type: GraphQLString },
        type: { type: GraphQLString },
    })
});

const OriginObjectType = new GraphQLObjectType({
    name: 'Origin',
    fields: ( ) => ({
        id: { type: GraphQLID },
        name: { type: GraphQLString },
        chemos: {
            type: new GraphQLList(ChemoObjectType),
            resolve(parent: { id: any; }, _args: any){
                return Chemo.find({ originId: parent.id });
            }
        }
    })
});

const RootQuery = new GraphQLObjectType({
    name: 'RootQueryType',
    fields: {
        chemo: {
            type: ChemoObjectType,
            args: { id: { type: GraphQLID } },
            resolve(_parent: any, args: { id: any; }){
                return Chemo.findById(args.id);
            }
        },
        origin: {
            type: OriginObjectType,
            args: { id: { type: GraphQLID } },
            resolve(_parent: any, args: { id: any; }){
                return Origin.findById(args.id);
            }
        },
        chemos: {
            type: new GraphQLList(ChemoObjectType),
            resolve(_parent: any, _args: any){
                return Chemo.find({});
            }
        },
        origins: {
            type: new GraphQLList(OriginObjectType),
            resolve(_parent: any, _args: any){
                return Origin.find({});
            }
        },
        chemicals: {
            type: new GraphQLList(ChemicalObjectType),
            resolve(_parent: any, _args: any){
                return Chemical.find({});
            }
        }
    }
});

const Mutation = new GraphQLObjectType({
    name: 'Mutation',
    fields: {
        addOrigin: {
            type: OriginObjectType,
            args: {
                name: { type: GraphQLString },
            },
            resolve(_parent: any, args: { name: any; }){
                let origin = new Origin({
                    name: args.name,
                });
                return origin.save();
            }
        },
        addChemo: {
            type: ChemoObjectType,
            args: {
                name: { type: new GraphQLNonNull(GraphQLString) },
                genome: { type: new GraphQLNonNull(GraphQLString) },
                climate: { type: new GraphQLNonNull(GraphQLString) },
                behavior: { type: new GraphQLNonNull(GraphQLString) },
                originId: { type: new GraphQLNonNull(GraphQLID) }
            },
            resolve(_parent: any, args: { name: any; genome: String; climate: String; behavior: String, description: String; originId: any; }){
                let geno = new Geno({
                    genome: args.genome
                });
                geno.save();
                
                let pheno = new Pheno({
                    climate: args.climate,
                    behavior: args.behavior
                });
                pheno.save();
                
                let chemo = new Chemo({
                    name: args.name,
                    genoId: geno.id,
                    phenoId: pheno.id,
                    // profileIds: profile.id,
                    originId: args.originId
                });
                return chemo.save();
            }
        },
        addChemoNewOrigin: {
            type: ChemoObjectType,
            args: {
                name: { type: new GraphQLNonNull(GraphQLString) },
                genome: { type: new GraphQLNonNull(GraphQLString) },
                climate: { type: new GraphQLNonNull(GraphQLString) },
                behavior: { type: new GraphQLNonNull(GraphQLString) },
                originName: { type: new GraphQLNonNull(GraphQLString) }
            },
            resolve(_parent: any, args: { name: any; genome: String; climate: String; behavior: String, description: String; originName: String; }){
                let geno = new Geno({
                    genome: args.genome
                });
                geno.save();
                
                let pheno = new Pheno({
                    climate: args.climate,
                    behavior: args.behavior
                });
                pheno.save();

                let origin = new Origin({
                    name: args.originName
                });
                origin.save();
                
                let chemo = new Chemo({
                    name: args.name,
                    genoId: geno.id,
                    phenoId: pheno.id,
                    // profileIds: profile.id,
                    originId: origin.id
                });
                return chemo.save();
            }
        },
        addPheno: {
            type: PhenoObjectType,
            args: { 
                climate: { type: new GraphQLNonNull(GraphQLString) },
                behavior: { type: new GraphQLNonNull(GraphQLString) } 
            },
            resolve(_parent: any, args: { climate: any; behavior: any; }){
                let pheno = new Pheno({
                    climate: args.climate,
                    behavior: args.behavior
                });
                return pheno.save();
            }
        },
        addGeno: {
            type: GenoObjectType,
            args: { 
                genome: { type: new GraphQLNonNull(GraphQLString) },
                maGenoId: { type: new GraphQLNonNull(GraphQLID) },
                paGenoId: { type: new GraphQLNonNull(GraphQLID) }
            },
            resolve(_parent: any, args: { genome: any; maGenoId: any; paGenoId: any; }){
                let geno = new Geno({
                    genome: args.genome,
                    maGenoId: args.maGenoId,
                    paGenoId: args.paGenoId
                });
                return geno.save();
            }
        },
        addChemical: {
            type: ChemicalObjectType,
            args: {
                name: { type: new GraphQLNonNull(GraphQLString) },
                formula: { type: new GraphQLNonNull(GraphQLString) },
                medical: { type: new GraphQLNonNull(GraphQLString) },
                benefit: { type: new GraphQLNonNull(GraphQLString) },
                risk: { type: new GraphQLNonNull(GraphQLString) },
                type: { type: new GraphQLNonNull(GraphQLString) },
            },
            resolve(_parent: any, args: { name: any; formula: any; medical: any; benefit: any; risk: any; type: any }) {
                let chemical = new Chemical({
                    name: args.name,
                    formula: args.formula,
                    medical: args.medical,
                    benefit: args.benefit,
                    risk: args.risk,
                    type: args.type
                });
                return chemical.save();
            }
        },
        addProfile: {
            type: ProfileObjectType,
            args: { 
                chemoId: { type: new GraphQLNonNull(GraphQLString) },
                chemicalId: { type: new GraphQLNonNull(GraphQLString) },
            },
            resolve(_parent: any, args: { chemoId: any; chemicalId: any; }){
                let profile = new Profile({
                    chemoId: args.chemoId,
                    chemicalId: args.chemicalId,
                });
                return profile.save();
            }
        }
    }
});

export default new GraphQLSchema({
    query: RootQuery,
    mutation: Mutation
});
