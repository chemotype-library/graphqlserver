import { Schema, model } from 'mongoose';

const profileSchema = new Schema({
    chemoId: String,
    chemicalId: String,
});

export default model('profile', profileSchema, 'profile');
