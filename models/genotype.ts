import { Schema, model } from 'mongoose';

const genotypeSchema = new Schema({
    genome: String,
    maGenoId: String,
    paGenoId: String,
});

export default model('genotype', genotypeSchema, 'genotype');
