import express from 'express';
import mongoose from 'mongoose';
import cors from 'cors';
import schema from './schema/schema';

const graphqlHTTP = require('express-graphql');
const app = express();

// allow cross-origin requests
app.use(cors());

// connect to mlab database username: admin, passwd: hJiBXZz0ktvNJaPQ
// make sure to replace my db string & creds with your own
mongoose.connect('mongodb+srv://admin:hJiBXZz0ktvNJaPQ@cluster0.kyn34.azure.mongodb.net/chemotype?retryWrites=true&w=majority')
mongoose.connection.once('open', () => {
    console.log('connected to database');
});

// bind express with graphql
app.use('/graphql', graphqlHTTP({
    schema,
    graphiql: true
}));

app.listen(4000, () => {
    console.log('now listening for requests on port 4000');
});
