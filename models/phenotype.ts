import { Schema, model } from 'mongoose';

const phenotypeSchema = new Schema({
    climate: String,
    behavior: String
});

export default model('phenotype', phenotypeSchema, 'phenotype');
