import { Schema, model } from 'mongoose';

const originSchema = new Schema({
    name: String,
    chemos: [{ type: Schema.Types.ObjectId, ref: 'chemotype' }]
});

export default model('origin', originSchema, 'origin');
